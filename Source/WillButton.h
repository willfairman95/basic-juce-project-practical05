//
//  WillButton.h
//  
//
//  Created by Will Fairman on 23/10/2015.
//
//

#ifndef ____WillButton__
#define ____WillButton__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class WillButton  : public Component
{
public:
    WillButton();
    ~WillButton();
    
    void paint (Graphics& g) override;
    
private:
    
};

#endif /* defined(____WillButton__) */
