/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    addAndMakeVisible(button1);
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    DBG("Width = " << getWidth() << " Height = " << getHeight());
    
    button1.setBounds(10, 10, getWidth(), getHeight());
    
}

void MainComponent::mouseUp(const MouseEvent& event)
{
    x = event.x;
    y = event.y;
    
    DBG("Mouse clicked!");
    
    repaint();
}

void MainComponent::paint(juce::Graphics &g)
{
    DBG("X = " << x << " Y = " << y);
    g.setColour(Colours::hotpink);
    g.drawEllipse(x, y, getWidth(), getHeight(), 10);
    
}
